﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;
using XamarinUniversity.Interfaces;
using XamarinUniversity.Services;

namespace Unit5_DataBinding
{
    public partial class App : Application
    {

        public App()
        {
            FormsNavigationPageService service = new FormsNavigationPageService();
            service.RegisterPage(AppPage.Info, () => new Info());
            service.RegisterPage(AppPage.Login, () => new Login(service));
            service.RegisterPage(AppPage.Product, () => new Products(service));
            service.RegisterPage(AppPage.ProductDetail, () => new ProductDetail());

            InitializeComponent();
            this.MainPage = new NavigationPage(new Products(service));

        }


    }
}
