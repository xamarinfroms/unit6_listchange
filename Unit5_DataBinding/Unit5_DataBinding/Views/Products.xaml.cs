﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unit5.Core;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XamarinUniversity.Interfaces;

namespace Unit5_DataBinding
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Products : ContentPage
    {
        public Products(INavigationService nav)
        {
            InitializeComponent();
            var viewModel = new ProductsViewModel() { NavigationService = nav };
            this.BindingContext = viewModel;


            Device.BeginInvokeOnMainThread(() =>
            {

            });

            BindingBase.EnableCollectionSynchronization(
                viewModel.Products, 
                null,
                (datas, context, action, writeAccess) =>
                {
                    lock (datas)
                    {
                        action();
                    }
                });
        }


    }
}
