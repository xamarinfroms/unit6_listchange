﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unit5.Core;

namespace Unit5_DataBinding
{
    public class ProductDetailViewModel
    {
        public Product ProductData { get; set; }
        public ProductDetailViewModel(Product productData)
        {
            this.ProductData = productData;
        }
    }
}
