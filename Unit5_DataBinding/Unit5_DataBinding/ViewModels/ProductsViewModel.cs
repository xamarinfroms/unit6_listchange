﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using Unit5.Core;
using Xamarin.Forms;
using XamarinUniversity.Interfaces;

namespace Unit5_DataBinding
{
    public class ProductsViewModel : INotifyPropertyChanged
    {
        public INavigationService NavigationService { get; set; }
        public ProductsViewModel()
        {
            var datas = ProductFactory.CreateProducts();
            ObservableCollection<Product> items = new ObservableCollection<Product>();
            foreach (var item in datas)
                items.Add(item);
            this.Products = items;
            this.OnUpdateData = new Command(()=>
            {
                this.Products.Insert(0, new Product() { Name = "Test" });
            });
          
        }

        public IList<Product> Products { get; private set; }
        public Product SelectedProduct
        {
            get { return _selectedProduct; }
              set{ this._selectedProduct = value;this.PropertyChanged?.DynamicInvoke(this, new PropertyChangedEventArgs(nameof(this.SelectedProduct))); }
        }

        public ICommand OnUpdateData { get; set; }



        private Product _selectedProduct;
        public event PropertyChangedEventHandler PropertyChanged;
    }
}
